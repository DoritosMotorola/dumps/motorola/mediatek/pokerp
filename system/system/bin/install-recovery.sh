#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:f7b6943dc877c96d451be7a3cde49367faab87ef; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:3583d1fdc3e5bc34ac8c32996b497debb5cf0f56 EMMC:/dev/block/platform/bootdevice/by-name/recovery f7b6943dc877c96d451be7a3cde49367faab87ef 33554432 3583d1fdc3e5bc34ac8c32996b497debb5cf0f56:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
